terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  
}


provider "aws" {

    region = "us-east-1"
}

#declaring an example vpc module found in terraform registry
resource "aws_default_vpc" "default-vpc" {

}

resource "aws_default_subnet" "default_subnet" {
  availability_zone = var.avail_zone

}


module "security_group_module" {
  source = "./modules/security_group"
  vpc_id = aws_default_vpc.default-vpc.id
  vpc_prefix = var.env_prefix
  vpc_cidr_block = aws_default_vpc.default-vpc.cidr_block
}


module "es_module" {
  source = "./modules/es"

  es_subnet_ids = [aws_default_subnet.default_subnet.id]

  source_security_group_ids = [
    module.security_group_module.es_security_group]

  instance_type = var.es_instance_type
  es_version = var.elastic_version
}




