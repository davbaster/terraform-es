# outputs values after the terraform apply is ran, the values belongs to the resources recently created
# you can do terraform plan in order to have a list of attributes to choose and use with output function
# you need to define one output for each attribute, you cant have two values in the same output
#you use the internal variable names
# output "myapp-vpc-id" {
#     value = module.vpc.vpc_id 
# }

output "myapp-default-subnet-id" {
    value = aws_default_subnet.default_subnet.id
}

# output "myapp-igw-id" {
#     value = aws_internet_gateway.myapp-igw.id
# }

# output "aws_ami_id" {
#     value = data.aws_ami.latest-amazon-linux-image.id
# }

# output "aws_instance_id" {
#     value = aws_instance.myapp-server.id
# }

# output "ec2_public_ip" {
#     value = module.myapp-server.instance.public_ip
# }