resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

resource "aws_elasticsearch_domain" "octane_es" {
  domain_name = var.domain_name
  elasticsearch_version = var.es_version
  cluster_config {
    dedicated_master_enabled = false
    instance_count = 1
    instance_type = var.instance_type
    zone_awareness_enabled = false
  }
  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

  vpc_options {
    security_group_ids = var.source_security_group_ids
    subnet_ids = slice (var.es_subnet_ids, 0, 1)
  }

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": "*",
            "Effect": "Allow",
            "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.domain_name}/*"
        }
    ]
}
CONFIG

  depends_on = [
    aws_iam_service_linked_role.es]
}