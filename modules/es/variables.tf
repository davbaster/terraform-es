variable "source_security_group_ids" {
  type = list(string)
}

variable "es_subnet_ids" {
  type = list(string)
}

variable "domain_name" {
  type = string
  default = "octane-es-domain"
}

variable "instance_type" {}
variable "es_version" {}