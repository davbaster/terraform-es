resource "aws_security_group" "es_security_group" {
  name = "${var.vpc_prefix}-elasticsearch-sg"
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "es_security_group_443" {
  type = "ingress"
  description = "allow from ec2 443"
  from_port = 443
  to_port = 443
  protocol = "tcp"
  security_group_id = aws_security_group.es_security_group.id
  cidr_blocks = [var.vpc_cidr_block]
}

resource "aws_security_group_rule" "es_security_group_80" {
  type = "ingress"
  description = "allow from ec2 80"
  from_port = 80
  to_port = 80
  protocol = "tcp"
  security_group_id = aws_security_group.es_security_group.id
  cidr_blocks = [var.vpc_cidr_block]
}

output "es_security_group" {
  value = aws_security_group.es_security_group.id
}

